// C++ code
//

int distanceThreshold = 0;
int status = 0;

long readUltrasonicDistance(int triggerPin, int echoPin)
{
  pinMode(triggerPin, OUTPUT);  // Clear the trigger
  digitalWrite(triggerPin, LOW);
  delayMicroseconds(2);
  // Sets the trigger pin to HIGH state for 10 microseconds
  digitalWrite(triggerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(triggerPin, LOW);
  pinMode(echoPin, INPUT);
  // Reads the echo pin, and returns the sound wave travel time in microseconds
  return pulseIn(echoPin, HIGH);
}

void setup()
{
  pinMode(8, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
}

void loop()
{
  distanceThreshold = 0;

  distanceThreshold = 0.01723 * readUltrasonicDistance(10,10); 

  if (distanceThreshold < 20) { 
    if (status < 1) {     
      digitalWrite(8, HIGH);
      analogWrite(3, random(255)); 
      analogWrite(1, random(255));
      analogWrite(2, random(255));  
      delay(5000);  
      digitalWrite(8, LOW);
      status = 1;         
    }
  }
  else {
    if (status > 0) {
      digitalWrite(3, LOW);
      digitalWrite(1, LOW);
      digitalWrite(2, LOW);   
      delay(1000);             
      digitalWrite(8, HIGH);
      delay(5000);             
      digitalWrite(8, LOW);    
      status = 0;         
    }
  }
}