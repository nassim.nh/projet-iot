# Projet IoT

Composants nécéssaire :
- Leds
- Poubelle classique
- Capteur ultrasons sous le couvercle
- Module Wi-Fi
- Leds RGB sur le contour intérieur de la poubelle
- Capteur à l'éxterieur pour décter l'utilisateur
- Liaisons electriques entre les capteur 
- Application mobile
- Arduino Uno

Pour le prototype nous avons simplement mis en place l'ouverture de la poubelle et l'alumage des leds via capteur ultrasons qui detecte un utilisateur qui s'approche a 20cm et se referme lorsqu'il part. 

https://www.tinkercad.com/things/bRDgXatRDra
